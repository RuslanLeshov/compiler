package ru.nsu.ojp2020;

import org.antlr.v4.runtime.tree.TerminalNode;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import static org.objectweb.asm.Opcodes.*;

public class CustomListener extends CustomLanguageBaseListener {
    private ClassWriter cw;
    private MethodVisitor mv;

    private final HashMap<String, Variable> variables = new HashMap<>();
    private final HashMap<String, Label> marks = new HashMap<>();
    private final HashSet<String> declaredMarks = new HashSet<>();
    private static int index = 1;
    private VariableType stackTopType = null;
    private Label skipLabel;

    @Override
    public void enterProgram(CustomLanguageParser.ProgramContext ctx) {

        // Начало записи класса. Называю класс Main, создаю метод main
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        String className = "Main";
        cw.visit(V1_8, ACC_PUBLIC, className, null, "java/lang/Object", null);

        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC,
                "main", "([Ljava/lang/String;)V",
                null, null);
        mv.visitCode();

        this.cw = cw;
        this.mv = mv;
    }

    @Override
    public void exitProgram(CustomLanguageParser.ProgramContext ctx) {
        // Конец записи класса. Пишу результат в файл
        if (!declaredMarks.equals(marks.keySet())) {
            throw new CompilerException("Goto mark not declared");
        }

        mv.visitInsn(RETURN);

        mv.visitMaxs(6, 6);
        mv.visitEnd();

        cw.visitEnd();

        try (FileOutputStream fileOutputStream = new FileOutputStream("Main.class")) {
            fileOutputStream.write(cw.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enterGotoMark(CustomLanguageParser.GotoMarkContext ctx) {
        // Объявление метки goto. Ставлю label в байткоде.
        final String markName = ctx.Identifier().getSymbol().getText();
        marks.putIfAbsent(markName, new Label());

        final Label label = marks.get(markName);
        mv.visitLabel(label);

        declaredMarks.add(markName);
    }

    @Override
    public void enterGotoStatement(CustomLanguageParser.GotoStatementContext ctx) {
        // Команда goto. Если метка не была объявлена раньше, то создаю лейбл (в байткод не пишу).
        final String markName = ctx.Identifier().getSymbol().getText();
        marks.putIfAbsent(markName, new Label());

        mv.visitJumpInsn(GOTO, marks.get(markName));
    }

    @Override
    public void enterVariableDeclaration(CustomLanguageParser.VariableDeclarationContext ctx) {
        // Объявление переменной. Заношу в мапу информацию о переменной.
        final TerminalNode name = ctx.Identifier();

        final String type = ctx.type().getText();
        VariableType variableType;
        if (ctx.type().intType() != null) {
            variableType = VariableType.INT;
        } else if (ctx.type().stringType() != null) {
            variableType = VariableType.STRING;
        } else {
            throw new CompilerException("Unknown type " + type);
        }
        variables.put(name.getSymbol().getText(), new Variable(name.getSymbol().getText(), index++, variableType));
    }


    @Override
    public void exitVariableAssignment(CustomLanguageParser.VariableAssignmentContext ctx) {

        // Присвоение переменной. Если правая часть присвоения это выражение от двух аргументов, то оно уже на стеке
        // Если нет, то заношу на стек.
        final Variable variable = getVariable(ctx.Identifier());

        if (ctx.expression() == null) {
            throw new CompilerException("Variable assignment is incorrect");
        }
        if (ctx.expression().oneArgExpression() != null) {
            final VariableType expressionType = getExpressionType(ctx.expression().oneArgExpression());
            loadArgument(ctx.expression().oneArgExpression(), expressionType);
        }

        if (variable.getType() != stackTopType) {
            throw new CompilerException("Cannot perform assignment to variable " + variable.getName());
        }
        stackTopType = null;

        switch (variable.getType()) {
            case INT:
                mv.visitVarInsn(ISTORE, variable.getIndex());
                break;
            case STRING:
                mv.visitVarInsn(ASTORE, variable.getIndex());
                break;
        }
    }


    @Override
    public void enterPrintStatement(CustomLanguageParser.PrintStatementContext ctx) {
        // Начало печати. Достаю System.out на стек.
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
    }

    @Override
    public void exitPrintStatement(CustomLanguageParser.PrintStatementContext ctx) {
        // Печать аргумента. Если аргумент это литерал или переменная, загружаем на стек.
        // В ином случае аргумент уже на стеке.
        if (ctx.expression().oneArgExpression() != null) {
            final VariableType type = getExpressionType(ctx.expression().oneArgExpression());
            loadArgument(ctx.expression().oneArgExpression(), type);
        }

        if (stackTopType == VariableType.INT) {
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V", false);
        } else if (stackTopType == VariableType.STRING) {
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        } else {
            throw new CompilerException("Illegal state");
        }
    }


    @Override
    public void exitSumExpression(CustomLanguageParser.SumExpressionContext ctx) {
        // Сумма. Если 2 инта, то складываем инты и кладем на стек.
        // Если хоть один из аргументов строка, то делаем конкатенацию строк и кладем на стек результат.
        final CustomLanguageParser.OneArgExpressionContext lhs = ctx.oneArgExpression(0);
        final CustomLanguageParser.OneArgExpressionContext rhs = ctx.oneArgExpression(1);

        final VariableType lhsType = getExpressionType(lhs);
        final VariableType rhsType = getExpressionType(rhs);

        final VariableType expressionType = getBinaryExpressionType(lhsType, rhsType);


        switch (expressionType) {
            case INT:
                loadArgument(lhs, lhsType);
                loadArgument(rhs, rhsType);
                mv.visitInsn(IADD);
                stackTopType = expressionType;
                return;

            case STRING:
                mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
                mv.visitInsn(DUP);
                mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
                loadArgument(lhs, lhsType);
                String descriptor = lhsType == VariableType.INT ? "(I)Ljava/lang/StringBuilder;" : "(Ljava/lang/String;)Ljava/lang/StringBuilder;";
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", descriptor, false);
                loadArgument(rhs, rhsType);
                descriptor = rhsType == VariableType.INT ? "(I)Ljava/lang/StringBuilder;" : "(Ljava/lang/String;)Ljava/lang/StringBuilder;";
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", descriptor, false);
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
                stackTopType = expressionType;
                return;
        }
        throw new CompilerException("Illegal state");
    }


    @Override
    public void exitSubExpression(CustomLanguageParser.SubExpressionContext ctx) {
        // Вычитаем два инта и кладем на стек
        final CustomLanguageParser.OneArgExpressionContext lhs = ctx.oneArgExpression(0);
        final CustomLanguageParser.OneArgExpressionContext rhs = ctx.oneArgExpression(1);

        writeBinaryIntExpression(lhs, rhs, ISUB);
    }

    @Override
    public void exitMulExpression(CustomLanguageParser.MulExpressionContext ctx) {
        // Умножение двух интов, результат на стек
        final CustomLanguageParser.OneArgExpressionContext lhs = ctx.oneArgExpression(0);
        final CustomLanguageParser.OneArgExpressionContext rhs = ctx.oneArgExpression(1);

        writeBinaryIntExpression(lhs, rhs, IMUL);
    }

    @Override
    public void exitDivExpression(CustomLanguageParser.DivExpressionContext ctx) {
        // Деление двух интов, результат на стек
        final CustomLanguageParser.OneArgExpressionContext lhs = ctx.oneArgExpression(0);
        final CustomLanguageParser.OneArgExpressionContext rhs = ctx.oneArgExpression(1);

        writeBinaryIntExpression(lhs, rhs, IDIV);
    }

    private void writeBinaryIntExpression(CustomLanguageParser.OneArgExpressionContext lhs,
                                          CustomLanguageParser.OneArgExpressionContext rhs, int opCode) {

        // Провести бинарную операцию с двумя интами
        final VariableType lhsType = getExpressionType(lhs);
        final VariableType rhsType = getExpressionType(rhs);

        final VariableType expressionType = getBinaryExpressionType(lhsType, rhsType);


        switch (expressionType) {
            case INT:
                loadArgument(lhs, lhsType);
                loadArgument(rhs, rhsType);
                mv.visitInsn(opCode);
                stackTopType = expressionType;
                return;

            case STRING:
                throw new CompilerException("Can't apply subtraction to strings");
        }
        throw new CompilerException("Illegal state");
    }

    @Override
    public void exitBooleanExpression(CustomLanguageParser.BooleanExpressionContext ctx) {
        // Вычисление значения булева выражения.

        final String operatorSymbol = ctx.booleanOperator().getText();
        boolean negate = ctx.negation() != null;

        final CustomLanguageParser.ExpressionContext lhs = ctx.expression(0);
        final CustomLanguageParser.ExpressionContext rhs = ctx.expression(1);
        if (lhs.oneArgExpression() != null) {
            final VariableType expressionType = getExpressionType(lhs.oneArgExpression());
            loadArgument(lhs.oneArgExpression(), expressionType);
        }
        if (rhs.oneArgExpression() != null) {
            final VariableType expressionType = getExpressionType(rhs.oneArgExpression());
            loadArgument(rhs.oneArgExpression(), expressionType);
        }
        int opcode;
        skipLabel = new Label();
        switch (operatorSymbol) {
            case "==":
                opcode = !negate ? IF_ICMPNE : IF_ICMPEQ;
                mv.visitJumpInsn(opcode, skipLabel);
                break;
            case "!=":
                opcode = !negate ? IF_ICMPEQ : IF_ICMPNE;
                mv.visitJumpInsn(opcode, skipLabel);
                break;
            case ">":
                opcode = !negate ? IF_ICMPLE : IF_ICMPGT;
                mv.visitJumpInsn(opcode, skipLabel);
                break;
        }
    }


    @Override
    public void exitConditionalStatement(CustomLanguageParser.ConditionalStatementContext ctx) {
        mv.visitLabel(skipLabel);
    }

    private VariableType getBinaryExpressionType(VariableType lhsType, VariableType rhsType) {
        // Вычисление типа выражения исходя из типа операндов
        final VariableType expressionType;
        if (lhsType == VariableType.STRING || rhsType == VariableType.STRING) {
            expressionType = VariableType.STRING;
        } else {
            if (lhsType != VariableType.INT || rhsType != VariableType.INT) {
                throw new CompilerException("Illegal state");
            }
            expressionType = VariableType.INT;
        }
        return expressionType;
    }

    private void loadArgument(CustomLanguageParser.OneArgExpressionContext arg, VariableType type) {
        // Положить аргумент на стек
        stackTopType = type;
        switch (type) {
            case INT:
                if (arg.Identifier() != null) {
                    final Variable variable = getVariable(arg.Identifier());
                    mv.visitVarInsn(ILOAD, variable.getIndex());
                    return;
                } else if (arg.literal() != null) {
                    final String text = arg.literal().IntLiteral().getSymbol().getText();
                    mv.visitLdcInsn(Integer.valueOf(text));
                    return;
                }
                break;
            case STRING:
                if (arg.Identifier() != null) {
                    final Variable variable = getVariable(arg.Identifier());
                    mv.visitVarInsn(ALOAD, variable.getIndex());
                    return;
                } else if (arg.literal() != null) {
                    final String text = arg.literal().StringLiteral().getText();
                    mv.visitLdcInsn(text.substring(1, text.length() - 1));
                    return;
                }
        }
        throw new CompilerException("Illegal state");
    }

    private VariableType getExpressionType(CustomLanguageParser.OneArgExpressionContext arg) {
        // Вычислить тип выражения

        if (arg.Identifier() != null) {
            Variable variable = getVariable(arg.Identifier());
            return variable.getType();
        } else if (arg.literal() != null) {
            if (arg.literal().IntLiteral() != null) {
                return VariableType.INT;
            } else if (arg.literal().StringLiteral() != null) {
                return VariableType.STRING;
            }
        }
        throw new CompilerException("Illegal state");
    }

    private Variable getVariable(TerminalNode identifier) {
        final String name = identifier.getSymbol().getText();
        final Variable variable = variables.get(name);
        if (variable == null) {
            throw new CompilerException("Variable " + name + " not declared.");
        }
        return variable;
    }
}
