package ru.nsu.ojp2020;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) {
        String filename = "input.txt";
        if (args.length == 1) {
            filename = args[0];
        }
        try (final InputStream is = Main.class.getClassLoader().getResourceAsStream("input.txt")) {
            if (is == null) throw new IOException();
            CustomLanguageLexer lexer = new CustomLanguageLexer(CharStreams.fromStream(is));

            CommonTokenStream tokens = new CommonTokenStream(lexer);
            CustomLanguageParser parser = new CustomLanguageParser(tokens);
            ParseTree tree = parser.program();

            ParseTreeWalker walker = new ParseTreeWalker();

            walker.walk(new CustomListener(), tree);
        } catch (IOException e) {
            System.out.println("Can't open file " + filename);
        }
    }
}
