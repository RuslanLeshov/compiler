package ru.nsu.ojp2020;

public class CompilerException extends RuntimeException {
    public CompilerException(String message) {
        super(message);
    }
}
