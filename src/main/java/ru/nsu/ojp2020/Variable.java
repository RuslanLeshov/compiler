package ru.nsu.ojp2020;

public class Variable {
    private final String name;
    private final int index;
    private final VariableType type;

    public Variable(String name, int index, VariableType type) {
        this.name = name;
        this.index = index;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public VariableType getType() {
        return type;
    }
}
