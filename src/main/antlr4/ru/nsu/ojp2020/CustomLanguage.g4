grammar CustomLanguage;

fragment LETTER: [a-zA-Z];
fragment DIGIT: [0-9];
fragment LETTER_OR_DIGIT: [a-zA-Z0-9];
fragment NUMBER: DIGIT | [1-9] DIGIT+;
fragment SIGN: [-]?;

Identifier: LETTER LETTER_OR_DIGIT*;


stringType: 'string';
intType: 'int';
type: intType | stringType;

literal: StringLiteral | IntLiteral;

StringLiteral: STRING_BOUNDARY STRING_CONTENT? STRING_BOUNDARY;
fragment STRING_CONTENT: STRING_CHAR+;
fragment STRING_CHAR: ~['];
fragment STRING_BOUNDARY: '\'';

IntLiteral: SIGN NUMBER;

variableDeclaration: Identifier ':' type;

variableAssignment: Identifier '=' expression;

expression: oneArgExpression | twoArgsExpression;
oneArgExpression: Identifier | literal;
twoArgsExpression: sumExpression | subExpression | mulExpression | divExpression;
sumExpression: oneArgExpression '+' oneArgExpression;
subExpression: oneArgExpression '-' oneArgExpression;
mulExpression: oneArgExpression '*' oneArgExpression;
divExpression: oneArgExpression '/' oneArgExpression;

statement: statementBody CRLF+;

statementBody: variableDeclaration | variableAssignment |
                conditionalStatement | gotoMark | gotoStatement | printStatement; // todo

conditionalStatement: 'if ' booleanExpression ':' statementBody;
booleanExpression: negation? expression booleanOperator expression;
negation: 'not ';
booleanOperator: '==' | '>' | '!=';

gotoMark: 'mark ' Identifier;
gotoStatement: 'goto ' Identifier;
printStatement: 'print ' expression;

program: statement+;

CRLF : '\r'? '\n' | '\r';

WS: [ \t]+ -> channel(HIDDEN);
